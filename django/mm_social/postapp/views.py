from django.http import HttpResponse
from django.shortcuts import render
from .models import Post


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def post_by_id(request, post_id):
    recent_post = Post.objects.get(id=post_id)
    return HttpResponse(recent_post.title + ': ' + recent_post.content)