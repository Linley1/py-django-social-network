from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^post/(?P<post_id>\d+)/$', views.post_by_id, name='post_by_id'),
]