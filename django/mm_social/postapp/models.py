import django
from django.db import models
# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    content = models.CharField(max_length=200)
    
    
class PostComment(models.Model):
    content = models.CharField(max_length=400)
    user = models.CharField(max_length=100)
    posted_on = models.DateTimeField(default=django.utils.timezone.now, blank=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    
    
class Tag(models.Model):
    tag = models.CharField(max_length=200)
    
    
class PostTag(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    
    
